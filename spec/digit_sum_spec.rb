require 'digit_sum'

describe DigitSum do 
  
  it "sum of the digits of all the numbers from 1 to N" do
    digit_sum = DigitSum.new
    expect(digit_sum.sum(10)).to eq(46)
  end
  
end