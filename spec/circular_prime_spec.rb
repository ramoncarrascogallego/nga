require 'circular_prime_count'

describe CircularPrimeCount do

  it "returns the number of primes circular between" do
    circular_prime_count = CircularPrimeCount.new
    expect(circular_prime_count.few_between(0,100)).to eq(13)
  end
  
end