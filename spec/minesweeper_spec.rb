require 'minesweeper'
describe Minesweeper do
  let(:string_pattern){"O O O O X O O O O O\nX X O O O O O O X O\nO O O O O O O O O O\nO O O O O O O O O O\nO O O O O X O O O O"}

  it "the minefield input created correctly" do
    minesweeper = Minesweeper.new(string_pattern)
    expect(minesweeper.minefield_input).to eq(["OOOOXOOOOO", "XXOOOOOOXO","OOOOOOOOOO","OOOOOOOOOO","OOOOOXOOOO"])
    expect(minesweeper.minefield_output).to eq(["0000X00000", "XX000000X0","0000000000","0000000000","00000X0000"])
  end

  it "calculates the number of mines properly" do
    minesweeper = Minesweeper.new(string_pattern)
    minesweeper.calculate_mine_count
    expect(minesweeper.minefield_output).to eq(["2211X10111","XX111101X1","2210000111","0000111000","00001X1000"])
  end
  
end