require 'missing_number'

describe MissingNumber do

  it "return [] if sequence is less than 1" do
    missing_number = MissingNumber.new("1")
    expect(missing_number.find_missing_numbers).to eq([])
  end

  it "return mising numbers of the sequence" do
    missing_number = MissingNumber.new("8:1,2,3,7,4,6,8")
    expect(missing_number.find_missing_numbers).to eq([5])
  end
  
end