require 'chessboard'

describe Chessboard do

  it "return [] if rows its 0" do
    chessboard = Chessboard.new(0,1)
    expect(chessboard.board).to eq([])
  end

  it "return [] if columns its 0" do
    chessboard = Chessboard.new(10,0)
    expect(chessboard.board).to eq([])
  end

  it "returns the correct board if the rows and columns are greater than 0" do
    chessboard = Chessboard.new(2,5)
    expect(chessboard.board).to eq(['01010','10101'])
  end

end