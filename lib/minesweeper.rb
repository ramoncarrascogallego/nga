class Minesweeper
  attr_reader :minefield_input, :minefield_output

  def initialize string_pattern
    @minefield_input = string_pattern.gsub(/ /,"").split("\n")
    @minefield_output = minefield_input.map{|s| s.gsub(/O/,"0")}
  end

  def calculate_mine_count
    (0..minefield_input.length - 1).each do |row|
        (0..minefield_input[row].length - 1).each do |column|
            calculate_number_of_mines_nearby(row,column) if is_a_mine?(minefield_input[row][column])
        end
    end
  end

private 

  def is_a_mine? value
    return value == "X"
  end

  def row_is_off_the_board? row
    row < 0 || row >= minefield_input.length 
  end

  def column_is_off_the_board? column
    column < 0 || column >= minefield_input[0].length
  end

  def increment_mine_nearby row, column
    mine_nearby = minefield_output[row][column].to_i + 1
    minefield_output[row][column] = mine_nearby.to_s
  end

  def calculate_number_of_mines_nearby row, column
    ((row - 1)..(row + 1)).each do |r|
        next if row_is_off_the_board?(r) 
        ((column-1)..(column+1)).each do |c|
            next if column_is_off_the_board?(c)
            increment_mine_nearby(r, c) unless is_a_mine?(minefield_input[r][c])
        end
    end 
  end
  
end