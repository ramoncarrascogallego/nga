class DigitSum

  def sum n
    (1..n).inject do |total,num| 
       total += num.to_s.chars.map(&:to_i).reduce(:+)
    end
  end

end
