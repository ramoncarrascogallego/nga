class Chessboard
  
  def initialize rows, columns
    @rows = rows
    @columns = columns
  end
    
  def board
    @board ||= compute_board
  end
    
private
    
  def compute_board
    return [] if impossible_board?
      
    [].tap do |board|
      @rows.times do |row|
        board << ("01" * @columns)[row, @columns]
      end
    end
  end
    
  def impossible_board?
    @rows.zero? || @columns.zero?
  end
  
end