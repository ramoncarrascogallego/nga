require 'prime'
class CircularPrimeCount

  def few_between min, max
    circular_primes = Prime.each(max).select do |prime|
      rotations(prime).all? { |prime_rotate| Prime.prime?(prime_rotate) } if prime > min
    end
    circular_primes.length
  end
 
private 
  
  def rotations prime
    digits = prime.to_s.chars
    digits.map { digits.rotate!.join.to_i }
  end

end
