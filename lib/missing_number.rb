class MissingNumber
  
  attr_reader :numbers_to_search, :max_numbers_to_search
  
  def initialize string_pattern   
    @max_numbers_to_search = string_pattern[0].to_i
    @numbers_to_search = string_pattern[2,string_pattern.length].split(",").map(&:to_i) if string_pattern.length > 3
  end

  def find_missing_numbers
    return [] if max_numbers_to_search <= 1
    (1..max_numbers_to_search).to_a - numbers_to_search
  end

end